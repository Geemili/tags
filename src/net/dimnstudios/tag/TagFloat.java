package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagFloat extends TagNumber {
	
	private float data;
	public TagFloat setData(float data) {
		this.data = data;
		return this;
	}
	public float getData() {return data;}

	@Override
	public Type getType() {
		return Type.TAG_Float;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeFloat(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readFloat();
	}
	@Override
	public void writeXML(Element element) {
		element.addText(Float.toString(data));
	}
	@Override
	public void readXML(Element element) {
		data = Float.parseFloat(element.getText());
	}
	
	
	
	@Override
	public double getNumberAsDouble() {
		return getData();
	}
	
	@Override
	public float getNumberAsFloat() {
		return getData();
	}
	
	@Override
	public int getNumberAsInt() {
		return (int) getData();
	}

	@Override
	public String getDataAsString() {
		return Float.toString(data);
	}

}
