package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.dom4j.Element;

public class TagList extends Tag {
	
	private ArrayList<Tag> data;
	public TagList setData(ArrayList<Tag> data) {
		this.data = data;
		return this;
	}
	public ArrayList<Tag> getData() {return data;}
	
	public TagList() {
		data = new ArrayList<Tag>();
	}
	
	public void addTag(Tag tag) {
		data.add(tag);
	}
	
	public void setTag(int index, Tag tag) {
		data.set(index, tag);
	}
	
	public void remove(int index) {
		data.remove(index);
	}
	
	public int size() {
		return data.size();
	}
	
	public Tag get(int index) {
		return data.get(index);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Tag> T getTag(int index, Class<T> clazz) {
		if(data.get(index).getClass()==clazz) {
			return (T) data.get(index);
		}
		return null;
	}

	@Override
	public Type getType() {
		return Type.TAG_List;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		byte type = 0;
		if(data.size()>0) data.get(0).getType().getId();
		out.writeByte(type);
		out.writeInt(data.size());
		for(Tag tag: data) {
			tag.write(out);
		}
	}

	@Override
	public void read(DataInput in) throws IOException {
		Type type = Type.getTypeById(in.readByte());
		int size = in.readInt();
		data = new ArrayList<Tag>(size);
		for(int index=0; index<size; index++) {
			Tag tag = getNewTag(type);
			tag.read(in);
			data.set(index, tag);
		}
	}
	@Override
	public void writeXML(Element element) {
		if(data.size()>0) {
			Type type = data.get(0).getType();
			element.addAttribute("type", type.getName());
			for(int index=0; index<data.size(); index++) {
				Element elementTag = element.addElement(type.getName());
				elementTag.addAttribute("index", Integer.toString(index));
				data.get(index).writeXML(elementTag);
			}
		} else {
			element.addAttribute("type", Tag.Type.TAG_End.getName());
		}
	}
	
	@Override
	public void readXML(Element element) {
		Type type = Type.getTypeByName(element.attributeValue("type"), true);
		if(type==Type.TAG_End)return;
		
		int size = element.nodeCount();
		data = new ArrayList<Tag>(size);
		Iterator<Element> iter = element.elementIterator(type.getName());
		while(iter.hasNext()) {
			Element next = iter.next();
			Tag t = Tag.getNewTag(type);
			t.readXML(next);
			data.add(t);
		}
	}
	
	@Override
	public String getDataAsString() {
		if(data.size()<1) return "[]";
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(data.get(0).getType().getName()+":");
		builder.append(data.size()+":");
		for(int index=0; index<data.size(); index++) {
			builder.append(data.get(index).getDataAsString());
			if(index!=data.size()-1) builder.append(",");
		}
		return builder.toString();
	}

}
