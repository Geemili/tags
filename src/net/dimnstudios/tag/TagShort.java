package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagShort extends TagNumber {
	
	private short data;
	public TagShort setData(short data) {
		this.data = data;
		return this;
	}
	public short getData() {return data;}

	@Override
	public Type getType() {
		return Type.TAG_Short;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeShort(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readShort();
	}
	@Override
	public void writeXML(Element element) {
		element.addText(Short.toString(data));
	}
	@Override
	public void readXML(Element element) {
		data = Short.parseShort(element.getText());
	}
	
	
	@Override
	public double getNumberAsDouble() {
		return getData();
	}
	@Override
	public float getNumberAsFloat() {
		return getData();
	}
	@Override
	public int getNumberAsInt() {
		return getData();
	}
	@Override
	public String getDataAsString() {
		return Short.toString(data);
	}

}
