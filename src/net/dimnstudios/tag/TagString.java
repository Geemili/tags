package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagString extends Tag {
	
	private String data = "";
	public TagString setData(String data) {
		this.data = data;
		return this;
	}
	public String getData() {return data;}

	@Override
	public Type getType() {
		return Type.TAG_String;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeUTF(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readUTF();
	}
	@Override
	public void writeXML(Element element) {
		element.addText(data);
	}
	@Override
	public void readXML(Element element) {
		data = element.getText();
	}
	
	@Override
	public String getDataAsString() {
		return data;
	}

}
