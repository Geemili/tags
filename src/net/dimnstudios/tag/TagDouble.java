package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagDouble extends TagNumber {

	private double data;
	public TagDouble setData(double data) {
		this.data = data;
		return this;
	}
	public double getData() {return data;}
	
	@Override
	public Type getType() {
		return Type.TAG_Double;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeDouble(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readDouble();
	}
	@Override
	public void writeXML(Element element) {
		element.addText(Double.toString(data));
	}
	@Override
	public void readXML(Element element) {
		data = Double.parseDouble(element.getText());
	}
	
	
	@Override
	public double getNumberAsDouble() {
		return getData();
	}
	@Override
	public float getNumberAsFloat() {
		return (float) getData();
	}
	@Override
	public int getNumberAsInt() {
		return (int) getData();
	}

	@Override
	public String getDataAsString() {
		return Double.toString(data);
	}

}
