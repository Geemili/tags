package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagInt extends TagNumber {

	private int data;
	public TagInt setData(int data) {
		this.data = data;
		return this;
	}
	public int getData() {return data;}
	
	@Override
	public Type getType() {
		return Type.TAG_Int;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readInt();
	}
	@Override
	public void writeXML(Element element) {
		element.addText(Integer.toString(data));
	}
	@Override
	public void readXML(Element element) {
		data = Integer.parseInt(element.getText());
	}
	
	
	@Override
	public double getNumberAsDouble() {
		return getData();
	}
	@Override
	public float getNumberAsFloat() {
		return getData();
	}
	@Override
	public int getNumberAsInt() {
		return getData();
	}

	@Override
	public String getDataAsString() {
		return Integer.toString(data);
	}

}
