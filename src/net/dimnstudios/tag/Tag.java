package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public abstract class Tag {
	
	public abstract Type getType();
	public abstract String getDataAsString();
	
	public abstract void write(DataOutput out) throws IOException;
	public abstract void read(DataInput in) throws IOException;
	
	public abstract void writeXML(Element element); //element would be the tags element
	public abstract void readXML(Element element);
	
	/// Start of static stuff
	
	public enum Type {
		TAG_End("End", 0),
		TAG_Byte("Byte", 1),
		TAG_Short("Short", 2),
		TAG_Int("Int", 3),
		TAG_Long("Long", 4),
		TAG_Float("Float", 5),
		TAG_Double("Double", 6),
		TAG_Byte_Array("ByteArray", 7),
		TAG_String("String", 8),
		TAG_List("List", 9),
		TAG_Compound("Compound", 10),
		TAG_Int_Array("IntArray", 11);
		
		private byte id;
		private String name;
		private Type(String name, int id) {
			this.name = name;
			this.id = (byte) id;
		}
		
		public byte getId() {
			return id;
		}
		
		public String getName() {
			return name;
		}
		
		public static Type getTypeById(int id) {
			for(Type t: values()) {
				if(t.getId()==id) {
					return t;
				}
			}
			return null;
		}
		
		public static Type getTypeByName(String name, boolean ignorecase) {
			for(Type t: values()) {
				if(t.getName().equals(name) || (ignorecase && t.getName().equalsIgnoreCase(name))) {
					return t;
				}
			}
			return null;
		}
	}
	
	public static TagCompound readFile(File file) throws IOException {
		FileInputStream in = new FileInputStream(file);
		TagCompound rootTag = readStream(in);
		in.close();
		return rootTag;
	}
	
	public static void writeToFile(File file, TagCompound compound) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		writeToStream(out, compound);
		out.close();
	}
	
	public static TagCompound readXMLFile(File file) throws IOException, DocumentException {
		FileInputStream in = new FileInputStream(file);
		TagCompound rootTag = readXMLStream(in);
		in.close();
		return rootTag;
	}
	
	public static void writeToXMLFile(File file, TagCompound compound, boolean prettyPrint) throws IOException {
		FileOutputStream out = new FileOutputStream(file); 
		writeToXMLStream(out, compound, prettyPrint);
		out.close();
	}
	
	public static TagCompound readStream(InputStream in) throws IOException {
		DataInputStream dataInputStream = new DataInputStream(in);
		TagCompound compound = new TagCompound();
		compound.read(dataInputStream);
		return compound;
	}
	
	public static TagCompound readXMLStream(InputStream in) throws IOException, DocumentException {
		SAXReader reader = new SAXReader();
		Document document = reader.read(in);
		Element compoundElement = document.getRootElement();
		TagCompound compound = new TagCompound();
		compound.readXML(compoundElement);
		return compound;
	}
	
	public static void writeToXMLStream(OutputStream out, TagCompound compound, boolean prettyPrint) throws IOException {
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement(Type.TAG_Compound.getName());
		compound.writeXML(root);
		
		XMLWriter writer;
		if(prettyPrint) {
			writer = new XMLWriter(out, OutputFormat.createPrettyPrint());
		} else {
			writer = new XMLWriter(out);
		}
		writer.write(document);
	}
	
	public static void writeToStream(OutputStream out, TagCompound compound) throws IOException {
		DataOutputStream dataOutputStream = new DataOutputStream(out);
		compound.write(dataOutputStream);
	}
	
	/*public static TagCompound readGzipStream(InputStream in) {
		
	}*/
	
	public static Tag getNewTag(Type type) {
		switch(type) {
		case TAG_End: return null;
		case TAG_Byte: return new TagByte();
		case TAG_Short: return new TagShort();
		case TAG_Int: return new TagInt();
		case TAG_Long: return new TagLong();
		case TAG_Float: return new TagFloat();
		case TAG_Double: return new TagDouble();
		case TAG_Byte_Array: return new TagByteArray();
		case TAG_String: return new TagString();
		case TAG_List: return new TagList();
		case TAG_Compound: return new TagCompound();
		case TAG_Int_Array: return new TagIntArray();
		}
		return null;
	}
	
}
