package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagIntArray extends Tag {

	private int[] data = new int[0];
	public TagIntArray setData(int[] data) {
		this.data = data;
		return this;
	}
	public int[] getData() {return data;}
	
	@Override
	public Type getType() {
		return Type.TAG_Int_Array;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(data.length);
		for(int i: data) {
			out.write(i);
		}
	}

	@Override
	public void read(DataInput in) throws IOException {
		int size = in.readInt();
		data = new int[size];
		for(int index=0; index<size; index++) {
			data[index] = in.readInt();
		}
	}
	@Override
	public void writeXML(Element element) {
		if(data.length<1) return;
		for(int index=0; index<data.length; index++) {
			element.addText(Integer.toString(data[index]));
			if(index<data.length-1) element.addText(",");
		}
	}
	
	@Override
	public void readXML(Element element) {
		if("".equals(element.getTextTrim())) return;
		
		String[] stringArray = element.getText().split(",");
		data = new int[stringArray.length];
		for(int index=0; index<stringArray.length; index++) {
			data[index] = Integer.parseInt(stringArray[index].trim());
		}
	}

	@Override
	public String getDataAsString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(data.length+":");
		for(int index=0; index<data.length; index++) {
			builder.append(data[index]);
			if(index!=data.length-1) builder.append(",");
		}
		builder.append("]");
		return builder.toString();
	}

}
