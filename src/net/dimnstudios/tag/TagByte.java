package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagByte extends TagNumber {
	
	private byte data;
	
	public TagByte setData(byte data) {
		this.data = data;
		return this;
	}
	
	public byte getData() {
		return data;
	}

	@Override
	public Type getType() {
		return Type.TAG_Byte;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readByte();
	}

	@Override
	public void writeXML(Element element) {
		element.addText(Byte.toString(data));
	}

	@Override
	public void readXML(Element element) {
		data = Byte.parseByte(element.getText());
	}

	
	
	@Override
	public double getNumberAsDouble() {
		return data;
	}

	@Override
	public float getNumberAsFloat() {
		return data;
	}

	@Override
	public int getNumberAsInt() {
		return data;
	}

	@Override
	public String getDataAsString() {
		return Byte.toString(data);
	}

}
