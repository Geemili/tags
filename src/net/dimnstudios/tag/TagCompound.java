package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.dom4j.Element;
import org.dom4j.Node;

public class TagCompound extends Tag {
	
	private HashMap<String, Tag> tags = new HashMap<String, Tag>();
	
	public TagCompound set(String name, Tag tag) {
		tags.put(name, tag);
		return this;
	}
	
	public Tag get(String name) {
		return tags.get(name);
	}
	
	public void remove(String name) {
		tags.remove(name);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Tag> T getTag(String name, Class<T> t) {
		Tag tag = get(name);
		if (tag != null) {
			if (tag.getClass() == t) {
				return (T) tag;
			}
		}
		return null;
	}
	
	public HashMap<String, Tag> getAllTags() {
		return tags;
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		for (Entry<String, Tag> tag : tags.entrySet()) {
			out.writeByte(tag.getValue().getType().getId());
			out.writeUTF(tag.getKey());
			tag.getValue().write(out);
		}
		out.writeByte(Type.TAG_End.getId());
	}
	
	@Override
	public void read(DataInput in) throws IOException {
		Type type;
		while ((type = Type.getTypeById(in.readByte())) != Type.TAG_End) {
			String name = in.readUTF();
			Tag tag = getNewTag(type);
			tag.read(in);
			tags.put(name, tag);
		}
	}
	
	@Override
	public Type getType() {
		return Type.TAG_Compound;
	}
	
	@Override
	public void writeXML(Element element) {
		if (tags.size() < 1)
			return;
		for (Entry<String, Tag> entry : tags.entrySet()) {
			entry.getValue().writeXML(
					element.addElement(entry.getValue().getType().getName())
							.addAttribute("name", entry.getKey()));
		}
	}
	
	@Override
	public void readXML(Element element) {
		for (int i = 0, size = element.nodeCount(); i < size; i++) {
			Node node = element.node(i);
			if (node instanceof Element) {
				Element tagsElement = (Element) node;
				Tag tag = Tag.getNewTag(Tag.Type.getTypeByName(
						tagsElement.getName(), false));
				tag.readXML(tagsElement);
				tags.put(tagsElement.attributeValue("name"), tag);
			}
		}
	}
	
	// ///////
	
	public void setString(String name, String value) {
		TagString string = new TagString();
		string.setData(value);
		set(name, string);
	}
	
	public String getString(String name) {
		TagString string = getTag(name, TagString.class);
		if (string != null) {
			return string.getData();
		}
		return null;
	}
	
	//
	public void setInt(String name, int value) {
		TagInt integer = new TagInt();
		integer.setData(value);
		set(name, integer);
	}
	
	public int getInt(String name) {
		TagInt integer = getTag(name, TagInt.class);
		if (integer != null) {
			return integer.getData();
		}
		return 0;
	}
	
	//
	public void setFloat(String name, float value) {
		TagFloat floatTag = new TagFloat();
		floatTag.setData(value);
		set(name, floatTag);
	}
	
	public float getFloat(String name) {
		TagFloat floatTag = getTag(name, TagFloat.class);
		if (floatTag != null) {
			return floatTag.getData();
		}
		return 0;
	}
	////
	public void setLong(String name, long value) {
		TagLong longTag = new TagLong();
		longTag.setData(value);
		set(name, longTag);
	}
	
	public float getLong(String name) {
		TagLong longTag = getTag(name, TagLong.class);
		if (longTag != null) {
			return longTag.getData();
		}
		return 0;
	}
	///
	
	@Override
	public String getDataAsString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (Entry<String, Tag> tag : tags.entrySet()) {
			builder.append(tag.getValue().getType().getName());
			builder.append(":");
			builder.append(tag.getKey());
			builder.append(":");
			builder.append(tag.getValue().getDataAsString());
			builder.append(",");
		}
		builder.append("]");
		return builder.toString();
	}
	
}
