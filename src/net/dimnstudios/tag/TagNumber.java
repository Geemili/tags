package net.dimnstudios.tag;


public abstract class TagNumber extends Tag {
	
	public abstract double getNumberAsDouble();
	public abstract float getNumberAsFloat();
	public abstract int getNumberAsInt();

}
