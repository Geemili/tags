package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagLong extends Tag {

	private long data;
	public TagLong setData(long data) {
		this.data = data;
		return this;
	}
	public long getData() {return data;}
	
	@Override
	public Type getType() {
		return Type.TAG_Long;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		data = in.readLong();
	}
	@Override
	public void writeXML(Element element) {
		element.addText(Long.toString(data));
	}
	@Override
	public void readXML(Element element) {
		data = Long.parseLong(element.getText());
	}
	@Override
	public String getDataAsString() {
		return Long.toString(data);
	}

}
