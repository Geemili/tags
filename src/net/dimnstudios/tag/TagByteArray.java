package net.dimnstudios.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.dom4j.Element;

public class TagByteArray extends Tag {

	private byte[] data = new byte[0];
	public TagByteArray setData(byte[] data) {
		this.data = data;
		return this;
	}
	public byte[] getData() {return data;}
	
	@Override
	public Type getType() {
		return Type.TAG_Byte_Array;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(data.length);
		out.write(data);
	}

	@Override
	public void read(DataInput in) throws IOException {
		int length = in.readInt();
		data = new byte[length];
		for(int i=0; i<length; i++) {
			data[i] = in.readByte();
		}
	}
	@Override
	public void writeXML(Element element) {
		if(data.length<1) return;
		for(int index=0; index<data.length; index++) {
			element.addText(Byte.toString(data[index]));
			if(index<data.length-1) element.addText(",");
		}
	}
	
	@Override
	public void readXML(Element element) {
		if("".equals(element.getTextTrim())) return;
		
		String[] stringArray = element.getText().split(",");
		data = new byte[stringArray.length];
		for(int index=0; index<stringArray.length; index++) {
			data[index] = Byte.parseByte(stringArray[index].trim());
		}
	}

	@Override
	public String getDataAsString() {
		return data.toString();
	}

}
